#pragma once

// ComputePipeline helps handling of URI loading 

// To use, instantiate a ComputePipeline and register actions to each state you want to suport
// registerAction accepts a state and a function pointer to an action
// When the pipeline is configured with actions at all suported states, 
// call load with a uri.
//
// load(std::string uri) returns a ComputeItem with the resulting state and data
// If the state is < 0, the load failed at some action stage.
// Otherwise the state indicates what kind of data is stored.


#include <string>
#include <stdint.h>
#include <cstring>
#include <vector>

using ComputeState = int32_t;

struct ComputeItem {
	std::vector<char> data;
	// Current state of item
	// -1 means undefined
	// The rest of the states are user defined
	ComputeState state = -1;
};

using ComputeAction = void(*)(ComputeItem&);

const ComputeState URI_STATE = 1;

class ComputePipeline
{
private:
	std::vector<ComputeAction> m_actions;

public:
	ComputePipeline()
	{
	}
	
	bool registerAction(ComputeState state, ComputeAction action)
	{
		m_actions.resize(state+1, nullptr);
		if (m_actions[state] != nullptr)
		{
			std::cout << "Error: trying to register multiple actions at state index '" << state << "'" << std::endl;
			return false;
		}
		m_actions[state] = action;
		return true;
	}

	ComputeItem load(std::string uri)
	{
		ComputeItem item;
		item.data = std::vector<char>(uri.begin(), uri.end());
		item.state = URI_STATE;
	
		ComputeState currentState = -1;
		while (item.state > 0)
		{
			if (item.state >= m_actions.size() || m_actions[item.state] == nullptr)
			{
				// When there are no more actions left, we are done
				break;
			}
			// We reset the state so that if anything goes wrong before the
			// action can set the new state, we will know
			currentState = item.state;
			item.state = -1;
			m_actions[currentState](item);
		}

		if (item.state < 0)
		{
			std::cout << "Error state after performing action at '" << currentState << "'\n";
		}

		return item;
	}
};
