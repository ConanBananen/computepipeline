#include <iostream>

#include "ComputePipeline.h"

const ComputeState FILE_PATH_STATE = 100;
const ComputeState FILE_STATE = 110;
const ComputeState LINK_STATE = 200;
const ComputeState BUNDLE_STATE = 300;
const ComputeState PNG_STATE = 1001;
const ComputeState JPG_STATE = 1002;

int main () 
{
	std::cout << "Testing compute pipeline\n\n";
	
	////////////////////////////////////////////////
	// Configure pipeline
	ComputePipeline pipeline;

	pipeline.registerAction(URI_STATE, [] (ComputeItem& item)
	{
		std::string uri = std::string((const char*)&item.data[0], item.data.size());
		std::cout << "Handling URI state, decide how to load uri '" << uri << "'" << std::endl;

		auto handleUri = [&] (const char* delimiter, ComputeState state)
		{
			if (!uri.starts_with(delimiter)) return false;

			uri.erase(0, std::strlen(delimiter));
			item.data = std::vector<char>(uri.begin(), uri.end());
			item.state = state;

			return true;
		};
		
		if (handleUri("file://", FILE_PATH_STATE)) return;
		if (handleUri("http://", LINK_STATE)) return;
		if (handleUri("https://", LINK_STATE)) return;
		if (handleUri("bundle://", BUNDLE_STATE)) return;
	});
	
	pipeline.registerAction(FILE_PATH_STATE, [] (ComputeItem& item)
	{
		std::string path = std::string((const char*)&item.data[0], item.data.size());
		if (path.ends_with(".png"))
		{
			// Load path and store png data into item.data...
			item.data = { 'P', 'N', 'G' };
			item.state = PNG_STATE;
			std::cout << "Loaded PNG data\n";
			return;
		}
	});
	
	////////////////////////////////////////////////
	
	auto pngFileResult = pipeline.load("file://myPng.png");
	if (pngFileResult.state < 0)
	{
		std::cout << "Failed to load png\n";
	}

	// Use png data...
}
