cmake_minimum_required(VERSION 3.5)

project(
	ComputePipelineTest
	VERSION 1.0
	LANGUAGES CXX)

add_executable(testComputePipeline main.cpp)

set_property(TARGET testComputePipeline PROPERTY CXX_STANDARD 20)
